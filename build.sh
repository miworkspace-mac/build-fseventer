#!/bin/bash -ex

# CONFIG
prefix="fseventer"
suffix=""
munki_package_name="fseventer"
display_name="fseventer"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o fseventer.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# unzip
unzip fseventer.zip

# make DMG from the inner prefpane
mkdir build-root
mv fseventer build-root
hdiutil create -srcfolder build-root -format UDZO -o app.dmg

mkdir build-app
mkdir build-app/Applications
mv "build-root/"* build-app/Applications

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-app/Applications/fseventer -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} --item="fseventer" --destinationpath="/Applications" | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-app//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`
version=`/usr/libexec/PlistBuddy -c "print :CFBundleVersion" "build-app/Applications/fseventer/fseventer.app/Contents/Info.plist"`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
